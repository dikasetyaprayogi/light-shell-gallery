### THE LIGHT SHELL JOURNEY

it all began when one stubborn, exaggerated, self proclaimed light advocate forge it's own path searching for wonderful world of light aesthetic beauty.


---


#### Light Shell 0

<img height="157" src="/demo/0.png">

[the defining moment](https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/5545###note_1701415) 

[a way to future](https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/2324) 


#### Gnome Shell Full Light Theme Experimentation

<img height="157" src="/demo/x.png">   <img height="157" src="/demo/xb.png">

[gnome gitlab page](https://gitlab.gnome.org/dikasp2/gnome-shell-light-theme-experimentation) 

[release announcement](https://discussion.fedoraproject.org/t/light-shell-for-gnome-desktop-experimentation/79656) 


#### Light Shell 42 (early concept)

<img height="157" src="/demo/42.png">   <img height="157" src="/demo/42b.png">

[gitlab page](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/lightshell-42) 


#### Light Shell 43 (pre development)

<img height="157" src="/demo/43.png">   <img height="157" src="/demo/43b.png">

[gitlab page](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/lightshell-43) 

 
#### Light Shell 44 (alpha)

<img height="157" src="/demo/44.png">   <img height="157" src="/demo/44b.png">

[gitlab page](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/lightshell-44) 

[spoiler](https://discussion.fedoraproject.org/t/light-shell-44-spoiler/84507) 

[release announcement](https://discussion.fedoraproject.org/t/light-shell-44-available-now/84675) 


#### Light Shell 44.2 - 44.2b (beta)

<img height="157" src="/demo/442.png">   <img height="157" src="/demo/442b.png">

[gitlab page](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/lightshell-44.2) 

[release announcement](https://discussion.fedoraproject.org/t/light-shell-44-2-bugfix-release-available-now/85164) 


#### Light Shell Extension Started

<img height="157" src="/demo/ext.png">   <img height="157" src="/demo/extb.png">

[release announcement](https://discussion.fedoraproject.org/t/light-shell-available-now-as-extension/85232) 

[pre development](https://discourse.gnome.org/t/need-help-creating-extension/16033) 


#### Light Shell 45 (pre-stable)

<img height="157" src="/demo/45.png">   <img height="157" src="/demo/45-1x.png">


#### Light Shell 45.1 (final-stable)

<img height="157" src="/demo/45-1a.png">   <img height="157" src="/demo/45-1b.png">

[gitlab page](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/lightshell-45.1?ref_type=heads)


#### Light Shell 45.2

<img height="157" src="/demo/45-2.png"> <img height="157" src="/demo/45-2x.png"> <img height="157" src="/demo/45-2m.png">

[gitlab](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/lightshell-45.2)

[release announcement](https://discussion.fedoraproject.org/t/light-shell-45-2-released/100102)

#### and to the Bright Future ...

<img src=/demo/fin.png>
